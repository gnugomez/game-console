# Un juego de cubos
Este es un proyecto programado completamente en javascript, y maquetado y estilizado con html y css.

Puedes probar el juego desde [este enlace](https://gnugomez.gitlab.io/game-console).

## Vista previa del proyecto, diseño mediante sketch
![alt text](images/preview.png)

# To Do
- [x] Añadir contador de vidas
- [x] Añadir mapas
- [x] Añadir mensajes al morir al ganar y demas
- [x] Permitir jugar desde un telefono móvil
